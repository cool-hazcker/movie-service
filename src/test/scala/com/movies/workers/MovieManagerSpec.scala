package com.movies.workers

import akka.testkit.{ ImplicitSender, TestKit }
import akka.actor.{ Actor, ActorRef, Props, ActorSystem }
import com.movies.database.{ RelationStat, DatabaseSupport }
import com.movies.scoring.ScoringSupport.MovieWithScore
import com.movies.workers.MovieProtocol.Movie
import org.scalatest.{ WordSpecLike, MustMatchers }

class MovieManagerSpec extends TestKit(ActorSystem("test"))
  with WordSpecLike
  with MustMatchers
  with ImplicitSender
  with StopSystemAfterAll {

  val storageMock = new DatabaseSupport() {
    val movie = Movie(Some("testId"), "TestMovie", 78, None)
    override def createMovie(title: String, rank: Double): Either[String, String] = (title, rank) match {
      case (movie.title, movie.rank) ⇒ Left(movie.id get)
      case _ ⇒ Right("Failure")
    }

    override def getAllMovies: Stream[Movie] = ???

    override def getAllRelated(from: String): Stream[RelationStat] = {
      RelationStat(Movie(Some("mov1"), "TestMovie1", 18, None), 1) #::
        RelationStat(Movie(Some("mov2"), "TestMovie2", 56, None), 2) #::
        RelationStat(Movie(Some("mov3"), "TestMovie3", 50, None), 2) #::
        RelationStat(Movie(Some("mov4"), "TestMovie4", 80, None), 3) #::
        Stream[RelationStat]()
    }

    override def getMovie(id: String): Option[Movie] = id match {
      case "testId" ⇒ Some(movie)
      case _ ⇒ None
    }

    override def createRelation(from: String)(to: List[String]): Either[String, String] = from match {
      case "testId" ⇒ Left("OK")
      case _ ⇒ Right("Failure")
    }
  }

  "MovieManager" must {

    "Create and get movies success messages are being received" in {
      import MovieProtocol._

      val movieMgr = system.actorOf(Props(new MovieManager(storageMock)))

      movieMgr ! AddMovie(storageMock.movie)
      expectMsg(MovieCreated(storageMock.movie.id get))

      movieMgr ! GetMovie(storageMock.movie.id get)
      expectMsg(Movie(Some("testId"), "TestMovie", 78, None))
    }

    "Create relations success messages are being received" in {
      import MovieProtocol._

      val movieMgr = system.actorOf(Props(new MovieManager(storageMock)))

      movieMgr ! AddRelations(storageMock.movie.id get, List("mov1", "mov2"))
      expectMsg(RelationCreated)
    }

    "Create movie failure messages are being received" in {
      import MovieProtocol._

      val movieMgr = system.actorOf(Props(new MovieManager(storageMock)))

      movieMgr ! AddMovie(Movie(Some("corruptedId"), "corruptedMov", 78, None))
      expectMsg(CreationFailed("Failure"))

    }

    "Create relations failure messages are being received" in {
      import MovieProtocol._

      val movieMgr = system.actorOf(Props(new MovieManager(storageMock)))

      movieMgr ! AddRelations("corruptedId", List("mov1", "mov2"))
      expectMsg(CreationFailed("Failure"))
    }

    "Get top 3 relations correct" in {
      import MovieProtocol._

      val movieMgr = system.actorOf(Props(new MovieManager(storageMock)))

      movieMgr ! GetTopNRelated(storageMock.movie.id get, 3)
      expectMsg(TopNRelated(
        List(
          MovieWithScore(Movie(Some("mov2"), "TestMovie2", 56, None), 28),
          MovieWithScore(Movie(Some("mov3"), "TestMovie3", 50, None), 25),
          MovieWithScore(Movie(Some("mov4"), "TestMovie4", 80, None), 20))))
    }

  }
}

