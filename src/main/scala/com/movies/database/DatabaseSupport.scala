package com.movies.database

import com.movies.workers.MovieProtocol.Movie

case class RelationStat(movie: Movie, distance: Int)

trait DatabaseSupport {
  def createMovie(title: String, rank: Double): Either[String, String]

  def createRelation(from: String)(to: List[String]): Either[String, String]

  def getMovie(id: String): Option[Movie]

  def getAllMovies: Stream[Movie]

  def getAllRelated(from: String): Stream[RelationStat]
}
