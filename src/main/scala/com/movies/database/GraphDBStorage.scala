package com.movies.database

import java.util.UUID

import com.movies.util.ConfigSupport
import com.movies.workers.MovieProtocol.Movie
import org.anormcypher.{ Cypher, Neo4jREST }

object GraphDBStorage {
  def apply() = new GraphDBStorage
}

class GraphDBStorage extends DatabaseSupport with ConfigSupport {
  //  protected val neo4jConf = config.getConfig("service.database")

  implicit lazy val connection = Neo4jREST()

  def createMovie(title: String, rank: Double) = {
    val uuid = UUID.randomUUID()
    val query = s"CREATE (m: Movie {uid: '$uuid', title: '$title', rank: $rank})"
    Cypher(query).execute() match {
      case true ⇒ Left(uuid.toString)
      case _ ⇒ Right("Adding movie failed")
    }
  }

  def createRelation(from: String)(to: List[String]) = {
    val query =
      s"""MATCH (root: Movie {uid: '$from'}), (related)
      | WHERE related.uid IN [${to map { "'" + _ + "'" } mkString (",")}]
      | WITH COLLECT(related) AS related_nodes, root AS start_node
      | FOREACH (end IN related_nodes |
      | MERGE (start_node)-[:RELATED_TO]->(end))""".stripMargin
    Cypher(query).execute() match {
      case true ⇒ Left("Successfully added relations")
      case _ ⇒ Right("Adding movie failed")
    }
  }

  def getMovie(id: String) = {
    val query =
      s"""MATCH (target: Movie {uid: '$id'})
         | RETURN target.uid, target.title, target.rank""".stripMargin
    val resultStream = Cypher(query).apply()
    resultStream map { row ⇒
      Movie(
        id = Some(row[String]("target.uid")),
        title = row[String]("target.title"),
        rank = row[Double]("target.rank"),
        related = None)
    } headOption
  }

  def getAllMovies = {
    val query =
      s"""MATCH (target: Movie)
         | RETURN target.uid, target.title, target.rank""".stripMargin
    val resultStream = Cypher(query).apply()
    resultStream map { row ⇒
      Movie(
        id = Some(row[String]("target.uid")),
        title = row[String]("target.title"),
        rank = row[Double]("target.rank"),
        related = None)
    }
  }

  // get all related nodes by ascending path length + remove duplicates + preserve order
  def getAllRelated(from: String) = {
    val query =
      s"""MATCH path=(from: Movie {uid: '$from'})-[:RELATED_TO*1..]-(related)
      |   WHERE related.uid <> from.uid
      |   RETURN  related.uid, related.title, related.rank, length(path) AS length
      |   ORDER BY length ASC""".stripMargin
    val resultStream = Cypher(query).apply()
    resultStream map { row ⇒
      RelationStat(
        Movie(
          id = Some(row[String]("related.uid")),
          title = row[String]("related.title"),
          rank = row[Double]("related.rank"),
          related = None),
        distance = row[Int]("length"))
    } filterNot {
      var set = Set[String]()
      rel: RelationStat ⇒
        val contains = set(rel.movie.title)
        set += rel.movie.title
        contains
    }
  }
}
