package com.movies.api

import akka.actor._
import akka.util.Timeout
import com.movies.database.GraphDBStorage
import com.movies.workers.MovieManager
import spray.http.StatusCodes
import spray.httpx.SprayJsonSupport._
import spray.routing.{ RequestContext, HttpServiceActor, HttpService, Route }
import scala.concurrent.duration._

class MovieService extends RestApi {

  def receive = runRoute(routes)
}

//trait StaticResources extends HttpService {
//
//  val staticResources =
//    get {
//      path("favicon.ico") {
//        complete(StatusCodes.NotFound)
//      } ~
//        path(Rest) { path =>
//          getFromResource("bootstrap/%s" format path)
//        } ~
//        path("file") {
//          getFromResource("application.conf")
//        }
//    }
//}
//
//trait TwirlPages extends HttpService {
//
//  val twirlPages =
//    get {
//      path("index") {
//        respondWithMediaType(`text/html`) {
//          complete(html.index().toString)
//        }
//      } ~
//        path("index2") {
//          respondWithMediaType(`text/html`) {
//            complete(html.index2("Spraying some Bootstrap", "Hello Twirl served by Spray").toString)
//          }
//        }
//    }
//}

trait RestApi extends HttpServiceActor {

  import com.movies.workers.MovieProtocol._
  import MovieJsonProtocol._

  import context.dispatcher

  implicit val timeout = Timeout(10 seconds)

  import akka.pattern.ask
  import akka.pattern.pipe

  val movieMgr = context.actorOf(Props(new MovieManager(GraphDBStorage())))

  val routes: Route =

    pathPrefix("movie") {
      get {
        pathEnd {
          requestContext ⇒
            val responder = createResponder(requestContext)
            (movieMgr ? GetAllMovies).pipeTo(responder)
        }
      } ~
        path(Segment) { id ⇒
          pathEnd {
            requestContext ⇒
              println("received get movie")
              val responder = createResponder(requestContext)
              (movieMgr ? GetMovie(id)).pipeTo(responder)
          }
        } ~
        path(Segment / "relevant") { id ⇒
          parameters('amount.as[Int]) { amount ⇒
            requestContext ⇒
              val responder = createResponder(requestContext)
              (movieMgr ? GetTopNRelated(id, amount)).pipeTo(responder)
          }
        } ~
        post {
          pathEnd {
            entity(as[Movie]) { movie ⇒
              requestContext ⇒
                val responder = createResponder(requestContext)
                (movieMgr ? AddMovie(movie)).pipeTo(responder)
            }
          } ~
            path(Segment / "relations") { from ⇒
              entity(as[List[String]]) { to ⇒
                requestContext ⇒
                  val responder = createResponder(requestContext)
                  (movieMgr ? AddRelations(from, to)).pipeTo(responder)
              }
            }
        }
    }

  def createResponder(requestContext: RequestContext) = {
    context.actorOf(Props(new Responder(requestContext, movieMgr)))
  }

  class Responder(requestContext: RequestContext, movieMgr: ActorRef) extends Actor with ActorLogging {

    import com.movies.workers.MovieProtocol._
    import spray.httpx.SprayJsonSupport._

    def receive = {

      case movie: Movie ⇒
        requestContext.complete(StatusCodes.OK, movie)
        self ! PoisonPill

      case movies: Movies ⇒
        requestContext.complete(StatusCodes.OK, movies)
        self ! PoisonPill

      case top: TopNRelated ⇒
        requestContext.complete(StatusCodes.OK, top)
        self ! PoisonPill

      case MovieCreated(id) ⇒
        requestContext.complete(StatusCodes.OK, id)
        self ! PoisonPill

      case RelationCreated ⇒
        requestContext.complete(StatusCodes.OK, "Successfully created relations")
        self ! PoisonPill

      case failure: CreationFailed ⇒
        requestContext.complete(StatusCodes.NotFound, failure)
        self ! PoisonPill

      case NotFound ⇒
        requestContext.complete(StatusCodes.NotFound)
        self ! PoisonPill

    }

  }
}
