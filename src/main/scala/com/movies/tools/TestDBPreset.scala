package com.movies.tools

import java.io.{ FileWriter, BufferedWriter }
import java.util.UUID

import org.anormcypher.{ Cypher, Neo4jREST }

object TestDBPreset extends App {
  implicit val connection = Neo4jREST()

  require(args.length > 0, "Output file not provided")
  val outputPath = args(0)

  object Closeables {
    def using[T <: { def close() }, R](resource: T)(block: T ⇒ R) = {
      try {
        block(resource)
      } finally {
        if (resource != null) {
          resource.close()
        }
      }
    }
  }

  def createMovie(title: String, rank: Double) = {
    val uuid = UUID.randomUUID()
    val query =
      s"CREATE (m: Movie {uid: '$uuid', title: '$title', rank: $rank})"
    Cypher(query).execute() match {
      case true ⇒ Left(uuid.toString)
      case _ ⇒ Right("Adding movie failed")
    }
  }

  val preset = List(
    ("Interstellar", 98),
    ("Armageddon", 56),
    ("Space Odyssey", 95),
    ("Gravity", 74),
    ("Gone girl", 85),
    ("Fizruk", 44),
    ("Kill Bill", 63))

  val list = preset map {
    case (title, rank) ⇒ {
      createMovie(title, rank) match {
        case Left(uid) ⇒ List(title, rank, uid)
        case Right(_) ⇒ List(title, rank, "ERROR: Not created")
      }
    }
  }

  Closeables.using(new BufferedWriter(new FileWriter(outputPath))) { file ⇒
    {
      file.write("TITLE\tRANK\tUID\n")
      list foreach { line ⇒
        file.write(line.mkString("\t") + "\n")
      }
    }
  }

}
