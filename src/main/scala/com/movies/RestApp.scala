package com.movies

import akka.actor.{Props, ActorSystem}
import akka.io.IO
import com.movies.api.MovieService
import spray.can.Http

/**
 * Created by Dan on 12.12.14.
 */
object RestApp extends App {
  implicit val actorSystem = ActorSystem("movie-app")

  val service = actorSystem.actorOf(Props[MovieService], "rest-service")

  IO(Http) ! Http.Bind(service, interface = "localhost", port = 8080)
}
