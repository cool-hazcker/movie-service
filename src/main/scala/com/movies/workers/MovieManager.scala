package com.movies.workers

import akka.actor.{ ActorLogging, Actor }
import com.movies.database.{ DatabaseSupport }
import com.movies.scoring.ScoringSupport
import com.movies.scoring.ScoringSupport.MovieWithScore
import spray.json.DefaultJsonProtocol

object MovieProtocol {
  import spray.json._

  case class Movie(id: Option[String], title: String, rank: Double, related: Option[List[String]])
  case class Movies(movies: List[Movie])
  case class AddMovie(movie: Movie)
  case class MovieCreated(uid: String)
  case class CreationFailed(msg: String)
  case object GetAllMovies
  case class AddRelations(from: String, to: List[String])
  case object RelationCreated
  case object NotFound
  case class GetMovie(id: String)
  case class GetTopNRelated(id: String, n: Int)
  case class TopNRelated(top: Seq[MovieWithScore])

  object MovieJsonProtocol extends DefaultJsonProtocol {
    implicit val movieFormat = jsonFormat4(Movie)
    implicit val moviesFormat = jsonFormat1(Movies)
    implicit val creationFormat = jsonFormat1(CreationFailed)
    implicit val moviesWithScoreFormat = jsonFormat2(MovieWithScore)
    implicit val topFormat = jsonFormat1(TopNRelated)
  }
}

class MovieManager(val storage: DatabaseSupport) extends Actor with ScoringSupport with ActorLogging {
  import MovieProtocol._

  def receive = {
    case AddMovie(movie) ⇒
      storage.createMovie(movie.title, movie.rank) match {
        case Left(uid) ⇒ sender ! MovieCreated(uid)
        case Right(failure) ⇒ sender ! CreationFailed(failure)
      }

    case GetAllMovies ⇒
      sender ! Movies(storage.getAllMovies.toList)

    case AddRelations(from, to) ⇒
      storage.createRelation(from)(to) match {
        case Left(_) ⇒ sender ! RelationCreated
        case Right(failure) ⇒ sender ! CreationFailed(failure)
      }

    case GetMovie(id) ⇒
      log.info(s"get movie with $id from actor")
      storage.getMovie(id) match {
        case Some(movie) ⇒
          log.info(s"get $movie from actor")
          sender ! movie
        case None ⇒
          log.info(s"nothing")
          sender ! NotFound
      }

    case GetTopNRelated(id, n) ⇒
      sender ! TopNRelated(getTopN(storage.getAllRelated(id).toList, n))
  }
}
