package com.movies.scoring

import com.movies.database.RelationStat
import com.movies.workers.MovieProtocol.Movie

import scala.collection.mutable

object ScoringSupport {
  case class MovieWithScore(movie: Movie, score: Double)
}

trait ScoringSupport {
  import ScoringSupport._
  def coefficient(distance: Int) = 1 / math.pow(2, distance - 1)

  // maintaining minheap
  private object OrderingByScore extends Ordering[ MovieWithScore ] {
    def compare(x: MovieWithScore, y: MovieWithScore) = y.score.compare(x.score)
  }

  def getTopN(stats: List[ RelationStat ], N: Int) = {
    val heap = mutable.PriorityQueue[ MovieWithScore ]()(OrderingByScore)
    val scores = stats map { s ⇒ MovieWithScore(s.movie, coefficient(s.distance) * s.movie.rank) }
    heap.enqueue((scores take N): _*)
    val restScores = scores drop N
    for (movieScore ← restScores; if heap.head.score < movieScore.score) {
      heap.dequeue()
      heap.enqueue(movieScore)
    }
    heap.dequeueAll.reverse
  }
}
