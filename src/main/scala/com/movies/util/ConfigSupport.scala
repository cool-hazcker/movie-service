package com.movies.util

import com.typesafe.config.ConfigFactory

/**
 * Created by Dan on 14.12.14.
 */
trait ConfigSupport {
  lazy val config = ConfigFactory.load()
}
