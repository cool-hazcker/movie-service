#!/bin/zsh

git clone git@bitbucket.org:cool-hazcker/movie-service.git
cd movie-service
brew install neo4j
neo4j start
sbt "run-main com.movies.tools.TestDBPreset movies_list.tsv"
# movies_list - список созданных пресетов фильмов
sbt "run-main com.movies.RestApp"